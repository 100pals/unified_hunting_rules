# _plugins/details_tag.rb

module Jekyll
  module Tags
    class DetailsTag < Liquid::Block

      def initialize(tag_name, markup, tokens)
        super
        @caption = markup
      end

      def render(context)
        caption_split = split_params(@caption)
        firstHeader = caption_split[0].strip
        secondHeader = caption_split[1].strip
        site = context.registers[:site]
        converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
        caption = converter.convert(firstHeader).gsub(/<\/?p[^>]*>/, '').chomp
        secondCaption = converter.convert(secondHeader).gsub(/<\/?p[^>]*>/, '').chomp
        body = converter.convert(super(context))
        "<details><summary>
        #{caption}
        #{secondCaption}
        </summary>#{body}</details>"
      end

      def split_params(params)
        params.split("|")
      end
    end
  end
end

Liquid::Template.register_tag('details', Jekyll::Tags::DetailsTag)
